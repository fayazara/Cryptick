package com.fayazahmed.cryptick.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.fayazahmed.cryptick.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class OneFragment extends Fragment{

    private static final String KOINEX_URL = "https://koinex.in/api/ticker";
    TextView a;
    ProgressBar progressBar;
    RequestQueue requestQueue;

    public OneFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_one, container, false);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
       //a = (TextView) getView().findViewById(R.id.textView);
       // a.setText("Koinex");
        progressBar = (ProgressBar) getView().findViewById(R.id.progressBarKoinex);
        final RequestQueue queue = Volley.newRequestQueue(getActivity().getApplicationContext());

        loadKoinex();
    }

    private void loadKoinex() {
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, KOINEX_URL, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // display response

                        JSONObject mainObject = null;
                        String bankDetails;
                        try {
                            JSONObject jsonObject = new JSONObject(response.toString());

                            //bankName.setText(jsonObject.getString("BANK"));
                            //ifscCode.setText(jsonObject.getString("IFSC"));
                            //branch.setText(jsonObject.getString("BRANCH"));
                            //contact.setText(jsonObject.getString("CONTACT"));
                            //address.setText(jsonObject.getString("ADDRESS") + ", " + jsonObject.getString("CITY") + ", " + jsonObject.getString("DISTRICT"));
                            //state.setText(jsonObject.getString("STATE"));
                            //bankDetails = "Bank: " + jsonObject.getString("BANK") + "\n\n" + "IFSC: " + jsonObject.getString("IFSC") + "\n\n" + "BRANCH: " + jsonObject.getString("BRANCH") + "\n\n" + "ADDRESS: " + jsonObject.getString("ADDRESS") + "\n\n" + "CONTACT: " + jsonObject.getString("CONTACT") + "\n\n" + "CITY: " + jsonObject.getString("CITY") + "\n\n" + "DISTRICT" + jsonObject.getString("DISTRICT") + "\n\n" + "STATE" + jsonObject.getString("STATE");
                            String tag = "Test";
                            Log.d(tag, jsonObject.toString());

                            Toast.makeText(getActivity().getApplicationContext(),jsonObject.toString(),Toast.LENGTH_SHORT);
                            //prg.setVisibility(View.GONE);
                            //crd.setVisibility(View.VISIBLE);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Error.Response", error.toString());
                    }
                }

        );
    }


}
